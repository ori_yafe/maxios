const methodTypes = {
  GET: "GET",
  POST: "POST",
  PATCH: "PATCH",
  DELETE: "DELETE",
  PUT: "PUT",
  ALL: "*",
};

export default methodTypes;
