import axios from "axios";
import methodTypes from "./methodTypes";

let middleWares = [];

maxios.use = (host = "*", fn = (body, options) => {}, method = "*") => {
  middleWares.push({
    host,
    fn,
    method
  });
};

let runMiddleWares = async (host = "", body = {}, options = {}, axiosMethod) => {
  await middleWares.forEach(async currMiddleWare => {
    if ((currMiddleWare.host === "*" || host.startsWith(currMiddleWare.host)) && (currMiddleWare.method === methodTypes.ALL || currMiddleWare === axiosMethod)) {
      await currMiddleWare.fn(body, options);
    }
  });
};

maxios.get = async (host = "", options = {}) => {
  await runMiddleWares(host, {}, options, methodTypes.GET);

  await axios.get(host, options);
};

maxios.delete = async (host = "", options = {}) => {
  await runMiddleWares(host, {}, options, methodTypes.DELETE);

  await axios.delete(host, options);
};

maxios.post = async (host = "", body = {}, options = {}) => {
  await runMiddleWares(host, body, options, methodTypes.POST);

  await axios.post(host, body, options);
};

maxios.put = async (host = "", body = {}, options = {}) => {
  await runMiddleWares(host, body, options, methodTypes.PUT);

  await axios.put(host, body, options);
};

maxios.patch = async (host = "", body = {}, options = {}) => {
  await runMiddleWares(host, body, options, methodTypes.PATCH);

  await axios.patch(host, body, options);
};

export default maxios;